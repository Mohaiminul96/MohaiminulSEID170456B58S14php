<!DOCTYPE html>
<html>
<head>
    <style>
        table, td, th {
            border: 2px solid black;

        }

        table {
            border-collapse: collapse;
            width: 100%;


        }

        th {
            text-align: left;
            color:palevioletred;
            background-color: beige;
        }

        td{
           background-color: antiquewhite;
        }
    </style>
</head>
<body>



<table>
    <tr>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Blood group</th>
        <th>ID</th>
    </tr>

    <tr>
        <td>karim</td>
        <td>ahmed</td>
        <td>A</td>
        <td>17272</td>
    </tr>
    <tr>
        <td>Rahim</td>
        <td>ahmed</td>
        <td>B</td>
        <td>47382</td>
    </tr>
    <tr>
        <td>Abul</td>
        <td>ahmed</td>
        <td>C</td>
        <td>43873</td>
    </tr>
    <tr>
        <td>Kabul</td>
        <td>ahmed</td>
        <td>D</td>
        <td>58335</td>
    </tr>

</table>

</body>
</html>
